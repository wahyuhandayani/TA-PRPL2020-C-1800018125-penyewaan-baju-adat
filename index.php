<?php  

	include 'layout/header.php';

	$baju = mysqli_query($con, "SELECT * FROM baju");

	if (isset($_GET['cari'])) {
		$key = $_GET['cari'];

		$cari = mysqli_query($con, "SELECT * FROM baju where jenis_baju like '%$key%' or harga like '%$key%' order by id_baju DESC");
	}
	else {
		$cari = $baju;
	}
?>
	<div class="jumbotron jumbotron-fluid">
	  <div class="container ">
	  </div>
	</div>

		<div class="container mb-4">
			<div class="row">
				<div class="col-9">
					<?php foreach($cari as $data):?>
					<div class="card mb-3" style="max-width: 100%;">
					  	<div class="row no-gutters">
					    	<div class="col-md-4">
					      		<img src="upload/<?=$data['foto'];?>" class="card-img" alt="...">
					    	</div>
					    	<div class="col-md-8">
					      		<div class="card-body">
					        		<h5 class="card-title"><?=$data['jenis_baju'];?></h5>
					        		<p class="card-text"><?=$data['penjelasan'];?></p>
					        		<p></p>
					        		<button type="button" class="btn warna mb-4" style="color: white;">Tersedia : <?=$data['stok'];?></button><br>
					        		<a href="membeli.php?id=<?=$data['id_baju'];?>" class="card-link btn btn-primary mt-5">Sewa</a>
					      		</div>
					    	</div>
					  	</div>
					</div>
					<?php  endforeach; ?>
				</div>
				<div class="col-3 border-left">
					<div>
						<form method="get" class=" mt-3">
							<div class="input-group mb-3 w-100">
								<input type="text" class="form-control" name="cari" placeholder="Pencarian">
								<div class="input-group-apend">
									<input type="submit"class="ml-3 w-100 h-100">
								</div>
							</div>
						</form>
					</div>
					<div class="mt-5">
						<h6>produk terlaris</h6>

						<?php
						//produk terlaris 
							$baju2 = mysqli_query($con, "SELECT * FROM baju order by stok ASC LIMIT 5"); 
							foreach($baju2 as $data2):
						?>
							<li><a href="membeli.php?id=<?=$data2['id_baju'];?>" style="color: black;"><?=$data2['jenis_baju'];?></a></li>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>

<?php  

	include 'layout/footer.php'

?>