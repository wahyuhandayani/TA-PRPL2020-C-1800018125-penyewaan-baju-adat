<?php  
    
    include "../config/koneksi.php";

    $err = '';
    if (isset($_POST['login']) && $_POST['login'] == "LOGIN") {
        
        $nama = $_POST['user'];
        $pass = md5($_POST['pass']);

        $sql = "SELECT * FROM admin where username = '$nama' and password = '$pass'";
        $query = mysqli_query($con, $sql);
    

        if (mysqli_num_rows($query) > 0) {
            session_start();
            $row = mysqli_fetch_array($query);
            $_SESSION['username'] = $row['username'];
            header('location:index.php');   
        }
        else{
            $err = "Username dan Password salah";
        }
    }

?>

<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="../assets/icon.png">
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="../css/style.css">

    <link rel="stylesheet" type="text/css" href="../font/css/all.css">
</head>
<body>
<center>
<div class="container">
<div class="col-md-4 col-md-offset-4 form-login">
    <div class="outter-form-login"style="background-color:yellow;">
      <form action="" class="inner-login" method="post">
            <h1 class="text-center title-login text-dark"><i class="fas fa-user"></i></h1><br>
                <div class="form-loginroup">
                    <input type="text" class="form-control" name="user" placeholder="Username">
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" name="pass" placeholder="Password">
                </div>
                
                <input type="submit" class="btn btn-block btn-dark" value="LOGIN" name="login">
                
                <div class="text-center forget">
                    <p>Forgot Password ?</p>
                </div>
            </form>
    </div>
</div>
</div>
</center>