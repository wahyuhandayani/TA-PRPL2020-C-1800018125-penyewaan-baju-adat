<?php  
	session_start();
	if ($_SESSION['username'] == '') {
		header("location:login.php");
	}

	include '../config/koneksi.php';

	include 'layout/header.php';

	$baju = mysqli_query($con, "SELECT * FROM baju");

	if (isset($_GET['cari'])) {
		$key = $_GET['cari'];

		$cari = mysqli_query($con, "SELECT * FROM baju where jenis_baju like '%$key%' or harga like '%$key%'");
	}
	else {
		$cari = $baju;
	}

?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<div class="chartjs-size-monitor" style="margin-top: 65px;">
		<h2>baju</h2>
		<form method="get" class=" mt-3">
				<div class="input-group mb-3 w-100">
					<input type="text" class="form-control" name="cari" placeholder="Pencarian">
					<div class="input-group-apend">
						<input type="submit"class="ml-3 w-100 h-100">
					</div>
				</div>
		</form>
		<table class="table">
			<thead class="warna text-white">
				<tr>
					<th scope="col">id_baju</th>
					<th scope="col">jenis_baju</th>
					<th scope="col">harga</th>
					<th scope="col">stok</th>
					<th scope="col">penjelasan</th>
					<th scope="col">foto</th>
					<th scope="col">Action</th>
				</tr>
			</thead>
			<tdody>
				<?php foreach($cari as $data):?>
				<tr>
					<th><?=$data['id_baju'];?></th>
					<td><?=$data['jenis_baju'];?></td>
					<td><?=$data['harga'];?></td>
					<td><?=$data['stok'];?></td>
					<td><?=$data['penjelasan'];?></td>
					<td><img src="../upload/<?=$data['foto'];?>" style="width: 100px"></td>
					<td>
						<a href="edit_jenis_baju.php?id=<?=$data['id_baju'];?>">Edit</a>
						<a href="hapus_baju.php?id=<?=$data['id_baju'];?>">Delete</a>
					</td>
				</tr>
			<?php endforeach;?>
			</tdody>
		</table>
		<a href="tambah_baju.php">Tambah baju</a>
	</div>
</main> 