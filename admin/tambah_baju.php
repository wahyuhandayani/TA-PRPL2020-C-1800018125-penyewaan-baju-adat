<?php  
	session_start();
	if ($_SESSION['username'] == '') {
		header("location:login.php");
	}

	include '../config/koneksi.php';

	include 'layout/header.php';

	$sql="SELECT MAX(id_baju) from baju";
    $hasil=mysqli_query($con, $sql);
    $data=mysqli_fetch_array($hasil);
       
    $idbaju = $data[0];
	$no_pinjam = (int) substr($idbaju,2,4);

	$no_pinjam++;

	$NewID = "AB".sprintf("%04s",$no_pinjam);

?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		<div class="chartjs-size-monitor" style="margin-top: 65px;">
			<form action="addjenis_baju.php" method="post" enctype="multipart/form-data">
				<h2>TAMBAH baju</h2>
				<div class="form-group-row">
					<label class="col-sm-2 col-form-label">id_baju</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control" id="formGroupExampleInput" name="id_baju" value="<?php echo "$NewID" ?>">
					</div>
				</div>
				<div class="form-group-row">
					<label class="col-sm-2 col-form-label">jenis_baju</label>
					<div class="col-sm-10">
						<input type="text" name="jenis_baju" class="form-control" placeholder="jenis_baju">
					</div>
				</div>
				<div class="form-group-row">
					<label class="col-sm-2 col-form-label">stok</label>
					<div class="col-sm-10">
						<input type="number" name="stok" class="form-control" placeholder="stok">
					</div>
				</div>
				<div class="form-group-row">
					<label class="col-sm-2 col-form-label">Penjelasan</label>
					<div class="col-sm-10">
						<textarea name="penjelasan" class="form-control" placeholder="penjelasan"></textarea>
					</div>
				</div>
				<div class="form-group-row">
					<label class="col-sm-2 col-form-label">harga </label>
					<div class="col-sm-10">
						<input type="text" name="harga" class="form-control" placeholder="harga">
					</div>
				</div>
				<div class="form-group-row">
					<label class="col-sm-2 col-form-label">Foto</label>
					<div class="col-sm-10">
						<input type="file" name="foto">
					</div>
				</div>
				<div class="form-group-row">
					<br><input type="submit" name="tambah_baju">
				</div>
			</form>
		</div>
</main>