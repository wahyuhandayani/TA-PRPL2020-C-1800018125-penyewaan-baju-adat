<?php  
	session_start();
	if ($_SESSION['username'] == '') {
		header("location:login.php");
	}

	include '../config/koneksi.php';

	include 'layout/header.php';

	$pembeli = mysqli_query($con, "SELECT * FROM Customer");

?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<div class="chartjs-size-monitor" style="margin-top: 65px;">
		<h2>Data Customer</h2>
		<table class="table">
			<thead class="warna text-black">
				<tr>
					<th scope="col">id_customer</th>
					<th scope="col">nama</th>
					<th scope="col">no_hp</th>
					<th scope="col">alamat</th>
					<th scope="col">jenis_kelamin</th>
				</tr>
			</thead>
			<tdody>
				<?php foreach($pembeli as $data):?>
				<tr>
					<th><?=$data['id_customer'];?></th>
					<td><?=$data['nama'];?></td>
					<td><?=$data['no_hp'];?></td>
					<td><?=$data['alamat'];?></td>
					<td><?=$data['jenis_kelamin'];?></td>
				</tr>
			<?php endforeach;?>
			</tdody>
		</table>
	</div>
</main>