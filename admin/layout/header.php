<?php 
	include '../config/koneksi.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>PRPL</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">

	<style type="text/css">
	  .warna{
	    background: chocolate !important;
	  }
	</style>
</head>
<body>
	
	<nav class="navbar navbar-dark fixed-top warna flex-md-nowrap shadow">
		<center><a class="navbar-brand col-sm-3 col-md-2 mr-0 font-weight-bold" href="../index.php">Penyewaan Baju Adat</a></center>
		<ul class="navbar-nav">
	    	<li class="nav-item">
				<a href="logout.php" class="nav-link text-white">Logout</a>
			</li>
	    </ul>
	</nav>
	<div class="container-fluid">
	  <div class="row">
	    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
	      <div class="sidebar-sticky">
					<ul class="nav flex-column">
						</li>
						<li class="nav-item">
							<a href="customer.php" class="nav-link" style="text-decoration: none; color: black;">Customer</a>
						</li>
						<li class="nav-item">
							<a href="jenis_baju.php" class="nav-link" style="text-decoration: none; color:black;">jenis_baju</a>
						</li>
						<li class="nav-item">
							<a href="transaksi.php" class="nav-link" style="text-decoration: none; color:black;">Transaksi</a>
						</li>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
