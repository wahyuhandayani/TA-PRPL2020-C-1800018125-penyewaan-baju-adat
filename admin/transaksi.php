<?php  
	session_start();
	if ($_SESSION['username'] == '') {
		header("location:login.php");
	}

	include '../config/koneksi.php';

	include 'layout/header.php';

	$pembeli = mysqli_query($con, "SELECT * FROM transaksi");

?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<div class="chartjs-size-monitor" style="margin-top: 65px;">
		<h2>transaksi</h2>
		<table class="table">
			<thead class="warna text-black">
				<tr>
					<th scope="col">id transaksi</th>
					<th scope="col">id baju</th>
					<th scope="col">id customer</th>
					<th scope="col">jumlah_sewa</th>
					<th scope="col">tanggal_sewa</th>
					<th scope="col">tanggal_pengembalian</th>
					<th scope="col">total</th>
					<th scope="col">Action</th>
				</tr>
			</thead>
			<tdody>
				<?php foreach($pembeli as $data):?>
				<tr>
					<th><?=$data['id_transaksi'];?></th>
					<td><?=$data['id_baju'];?></td>
					<td><?=$data['id_customer'];?></td>
					<td><?=$data['jumlah_sewa'];?></td>
					<td><?=$data['tanggal_sewa'];?></td>
					<td><?=$data['tanggal_pengembalian'];?></td>
					<td><?=$data['total'];?></td>
					<td>
						<a href="edit_transaksi.php?id=<?=$data['id_transaksi'];?>">Edit</a>
						<a href="hapus_trasaksi.php?id=<?=$data['id_transaksi'];?>">Delete</a>
					</td>
				</tr>
			<?php endforeach;?>
			</tdody>
		</table>
	</div>
</main>